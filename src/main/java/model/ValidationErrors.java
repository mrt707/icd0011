package model;

import java.util.ArrayList;
import java.util.List;

public class ValidationErrors {

    private List<ValidationError> errors = new ArrayList<>();

    public void addErrorMessage(ValidationError ve) {
        errors.add(ve);
    }

    public List<ValidationError> getErrors() {
        return errors;
    }

    public void setErrors(List<ValidationError> errors) {
        this.errors = errors;
    }
}
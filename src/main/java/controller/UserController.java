package controller;

import dao.UserDao;
import model.User;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@RestController
public class UserController {

    @GetMapping("/")
    public String frontPage() {
        return "Front page!";
    }

    @GetMapping("/version")
    public String version() {
        return "Ver 0.1 Alpha";
    }

    @GetMapping("/count")
    public String counter(HttpSession session) {

        Object count = session.getAttribute("count");

        count = count instanceof Integer
                ? (Integer) count + 1
                : 0;

        session.setAttribute("count", count);

        return String.valueOf(count);
    }

    @GetMapping("/home")
    public String home() {
        return "Api home url";
    }

    @GetMapping("/info")
    public String info(Principal principal) {
        String user;
        if (principal == null) {
            user = "";
        } else {
            user = principal.getName();
        }

        return "Current user: " + user;
    }

    @GetMapping("/admin/info")
    public String adminInfo(Principal principal) {
        return "Admin user info: " + principal.getName();
    }

    @GetMapping("/users")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public String getUsers() {
        return new UserDao().getAll();
    }

    @GetMapping("/users/{userName}")
    @PreAuthorize("#userName == authentication.name or hasAnyRole('ADMIN')")
    public User getUserByName(@PathVariable String userName) {
        return new UserDao().getUserByUserName(userName);
    }
}


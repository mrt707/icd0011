package controller;

import dao.OrderDao;
import model.Order;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    private OrderDao dao;

    public OrderController(OrderDao dao) {
        this.dao = dao;
    }

    @GetMapping("orders")
    public List<Order> getPosts() {
        //int i = 1/0;

        return dao.findOrders();
    }

    @PostMapping("orders")
    public Order createPost(@RequestBody @Valid Order order) {
        return dao.insertOrder(order);
    }

    @GetMapping("orders/{id}")
    public Order getPost(@PathVariable Long id) {
        return dao.findOrderById(id);
    }


    @DeleteMapping("orders/{id}")
    public void deletePost(@PathVariable Long id) {
        dao.deleteOrderById(id);
    }
}

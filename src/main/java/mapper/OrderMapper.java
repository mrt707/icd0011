package mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Order;
import util.Util;

import java.util.LinkedHashMap;
import java.util.Map;

public class OrderMapper {

    public Order parse(String input) throws Exception {
        Order order;
        if (input.contains("orderRows")) {
            order = new ObjectMapper().readValue(input, Order.class);
        }
        else {

            Map<String, String> map = parseToMap(input);

            order = new Order();
            order.setOrderNumber(map.get("orderNumber"));
        }


        return order;
    }


    public Map<String, String> parseToMap(String input) {
        input = input.replace("{", "");
        input = input.replace("}", "");

        String[] keyValuePairs = input.split(",");

        Map<String, String> map = new LinkedHashMap<>();

        /* Debug
        System.out.println(Arrays.toString(keyValuePairs));
        */

        for (String keyValuePair : keyValuePairs) {
            String[] keyAndValue = keyValuePair.split(":");

            String key = keyAndValue[0].replace("\"", "").trim();
            String value = keyAndValue[1].replace("\"", "").trim();

            map.put(key, value);


        }
        return map;
    }

    public String stringify(Order order) throws Exception{

        if (order.getOrderRows() != null) {
            return new ObjectMapper().writeValueAsString(order);
        }

        Map<Object, Object> map = new LinkedHashMap<>();
        map.put("id", order.getId().toString());
        map.put("orderNumber", order.getOrderNumber());

        return stringifyMap(map);


    }

    public String stringifyMap(Map<Object, Object> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Object, Object> pair : map.entrySet()) {
            sb.append("\"");
            sb.append(pair.getKey());
            sb.append("\"");
            sb.append(":");

            if (Util.isNumeric(pair.getValue().toString())) {
                sb.append(pair.getValue());
            }
            else {
                sb.append("\"");
                sb.append(pair.getValue());
                sb.append("\"");
            }



            sb.append(", ");



        }

        // Remove last char

        String result = sb.toString().trim();
        result = Util.removeLastChar(result);
        return String.format("{%s}", result);
    }
}

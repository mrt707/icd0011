package mapper;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnyMapper {


    private Logger logger = Logger.getLogger(AnyMapper.class.getName());

    // Method parses String and create object of Class
    public Object parse(String input, Class c) {

        Object obj = attemptClassInstantiation(c);

        // Prepare & clean string and create map out of it
        String cleanString = input.replace("{", "").replace("}", "").trim();
        Map<String, String> map = parseStringToMap(cleanString);

        // For every key-value pair, check props and invoke setter if matched with map
        assignClassProperties(map, obj);

        return obj;

    }

    // If property matches with map key, assign value
    private void assignClassProperties(Map<String, String> map, Object obj) {

        // Analyze bean and get property descriptors
        PropertyDescriptor[] pds = getClassProperties(obj.getClass());

        for (Map.Entry<String, String> entry : map.entrySet()) {

            Method setterMethod;
            for (PropertyDescriptor pd : pds) {
                setterMethod = pd.getWriteMethod(); // For Setter Method

                if (pd.getName().equals(entry.getKey().trim()) && setterMethod != null) {
                    setterMethod.setAccessible(true);

                    invokeClassMethod(obj, pd, entry.getValue().trim());

                } else {
                    continue;
                }
            }
        }

    }
    // Use setters provided by class
    private void invokeClassMethod(Object obj, PropertyDescriptor pd, String entry) {

        Method setterMethod = pd.getWriteMethod();
        try {
            if (pd.getPropertyType().toString().equals("int")) {
                setterMethod.invoke(obj, Integer.valueOf(entry));
            } else if (pd.getPropertyType().getTypeName().equals("java.lang.Long")) {
                if (!entry.equals("null")) {
                    setterMethod.invoke(obj, Long.valueOf(entry));
                }
            } else {
                setterMethod.invoke(obj, entry);
            }

        } catch (IllegalAccessException e) {
            logger.log(Level.WARNING, "Illegal Access Exception");
        } catch (InvocationTargetException e) {
            logger.log(Level.WARNING, "Invocation Target Exception");
        }
    }
    // Try to instantiate a class
    private Object attemptClassInstantiation(Class c) {

        Object obj = null;
        try {
            // Create new instance of object
            obj = Class.forName(c.getName()).getDeclaredConstructor().newInstance();
        } catch (InstantiationException e) {
            logger.log(Level.WARNING, "Instantiation Exception");
        } catch (IllegalAccessException e) {
            logger.log(Level.WARNING, "Illegal Access Exception");
        } catch (ClassNotFoundException e) {
            logger.log(Level.WARNING, "Class NF Exception");
        } catch (NoSuchMethodException e) {
            logger.log(Level.WARNING, "No Such Method Exception");
        } catch (InvocationTargetException e) {
            logger.log(Level.WARNING, "Invocation Target Exception");
        }

        return obj;
    }

    // Save key value pairs into map
    public Map<String, String> parseStringToMap(String cleanString) {
        Map<String, String> map = new HashMap<>();
        String[] keyValuePairs = cleanString.split(",");
        for (String pair : keyValuePairs) {
            String[] keyValue = pair.split(":");
            map.put(keyValue[0].replace("\"", ""), keyValue[1].replace("\"", ""));
        }
        return map;
    }

    //{name: "John", age: 30, city: "New York"};
    public String stringify(Object c) throws InvocationTargetException, IllegalAccessException {

        PropertyDescriptor[] pds = getClassProperties(c.getClass());

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Method getterMethod;
        for (PropertyDescriptor pd : pds) {

            if (pd.getPropertyType().toString().equals("class java.lang.Class")) {
                continue;
            }
            getterMethod = pd.getReadMethod(); // For Getter Method

            sb.append("\"" + pd.getName() + "\": ");


            if ("int".equals(pd.getPropertyType().toString())) {
                sb.append(getterMethod.invoke(c)).append(", ");
            } else {
                sb.append("\"" + getterMethod.invoke(c)).append("\", ");
            }
        }

        // Remove last char
        sb.setLength(sb.length() - 2);
        sb.append("}");
        return sb.toString();
    }

    private PropertyDescriptor[] getClassProperties(Class c) {
        BeanInfo beaninfo = null;
        try {
            beaninfo = Introspector.getBeanInfo(c);
        } catch (IntrospectionException e) {
            logger.log(Level.WARNING, "Introspection Exception");
        }
        return beaninfo.getPropertyDescriptors();
    }
}

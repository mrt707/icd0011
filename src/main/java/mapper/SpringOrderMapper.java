package mapper;

import model.Order;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SpringOrderMapper implements RowMapper<Order> {
        @Override
        public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
            Order o = new Order();

            o.setId(rs.getLong("id"));
            o.setOrderNumber(rs.getString("orderNumber"));

            return o;
        }

}

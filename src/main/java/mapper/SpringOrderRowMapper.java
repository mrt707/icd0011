package mapper;

import model.OrderRow;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SpringOrderRowMapper implements RowMapper<OrderRow> {
    @Override
    public OrderRow mapRow(ResultSet rs, int rowNum) throws SQLException {
        OrderRow or = new OrderRow();

        or.setItemName(rs.getString("itemName"));
        or.setPrice(rs.getInt("price"));
        or.setQuantity(rs.getInt("quantity"));

        return or;
    }

}

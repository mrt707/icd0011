package dao;

import model.Order;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.sql.*;
import java.util.List;

@Repository
public class OrderDao {

    @PersistenceContext
    private EntityManager em;


    private static final String ON = "ordernumber";



    @PostConstruct
    public void init() {
    }

    @Transactional
    public Order insertOrder(Order o) {

        if (o.getId() == null) {
            em.persist(o);
        }
        else {
            em.merge(o);
        }
        return o;
    }

    public List<Order> findOrders() {

        return em.createQuery("select o from Order o").getResultList();

    }


    private static class OrderMapper implements RowMapper<Order> {
        @Override
        public Order mapRow(ResultSet rs, int rowNum) throws SQLException {

            Order o = new Order();

            o.setId(rs.getLong("id"));
            o.setOrderNumber(rs.getString(ON));

            return o;
        }
    }


    public Order findOrderById(Long id) {
        TypedQuery<Order> query = em.createQuery(
                "select o from Order o where o.id = :id",
                Order.class);

        query.setParameter("id", id);

        return query.getSingleResult();



    }

    @Transactional
    public void deleteOrderById(Long id) {
        int deleted = em.createQuery("delete from Order o where o.id=:id")
                .setParameter("id", id)
                .executeUpdate();
        if (deleted == 0) {
            throw new OptimisticLockException("Can't do");
        }

    }



}